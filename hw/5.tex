\documentclass[parskip]{scrartcl}
\usepackage[margin=1in]{geometry}
\usepackage[T1]{fontenc}
\usepackage{fancyvrb}
\usepackage{amsfonts}
\usepackage{enumitem}
\usepackage{tikz}
\usepackage{hyperref}
\usepackage{MnSymbol}

\subject{CS 311 Computational Structures}
\author{Katie Casamento}
\date{Spring 2023}
\publishers{Portland State University}

\title{Assignment 5}
\subtitle{Turing machines}

% straight quotes in \mathtt
\DeclareMathSymbol{\mathdblquotechar}{\mathalpha}{letters}{`"}
\newcommand{\mathdblquote}{\mathtt{\mathdblquotechar}}
\begingroup\lccode`~=`"\lowercase{\endgroup
  \let~\mathdblquote
}
\AtBeginDocument{\mathcode`"="8000 }

\begin{document}

\maketitle

Each exercise is worth \textbf{four} points, for a total of 36 points. (Same point total as the other assignments, but fewer questions.)

If you're working on paper, it's fine to ``scan'' your work with a phone camera. Just \textbf{make sure to set the lighting and contrast so that your writing is clearly readable}.

If you're working on a computer, you might find this tool helpful:

\quad \url{https://madebyevan.com/fsm/}

There is at least one useful feature of this tool that is not mentioned by the instructions on the webpage: you can click and drag the \textbf{middle} of an arrow to make it curved.

Feel free to submit your diagrams as \texttt{.png} files, just \textbf{make sure to name the files so that it's clear which exercise each image corresponds to}.

\section{Exercises}

For exercises 1-4, draw a diagram to define a \textbf{Turing machine} (not a DFA or NFA) which \textbf{decides} the language (gives a ``yes'' or ``no'' answer in \textbf{finite time} for all possible inputs).

\begin{enumerate}[noitemsep]
  \item $\{ \mathtt{a^n\ |\ n}$ is even$\}$
  \item $\{ \mathtt{a^mb^n\ |\ m > n} \}$
  \item $\{ \mathtt{a^nbc^n} \}$
  \item $\{ \mathtt{w\ |\ w \in \{a,b\}^*}$ and $\mathtt{w}$ contains the same number of $\mathtt{a}$s and $\mathtt{b}$s$\}$
\end{enumerate}

For each Turing machine diagram in exercises 5-7, list \textbf{three} strings in the language recognized by the Turing machine. For every machine, the input alphabet is $\{ \mathtt{a}, \mathtt{b}, \mathtt{c} \}$.

\begin{enumerate}[resume]
  \item
    \begin{tikzpicture}[scale=0.2,baseline={([yshift=-.5ex]current bounding box.center)}]
    \tikzstyle{every node}+=[inner sep=0pt]
    \draw [black] (36.5,-15.6) circle (3);
    \draw [black] (53.9,-15.6) circle (3);
    \draw [black] (53.9,-15.6) circle (2.4);
    \draw [black] (27.6,-15.6) -- (33.5,-15.6);
    \fill [black] (33.5,-15.6) -- (32.7,-15.1) -- (32.7,-16.1);
    \draw [black] (35.177,-12.92) arc (234:-54:2.25);
    \draw (36.5,-8.35) node [above] {\begin{tabular}{c}$\mathtt{a\mbox{ }/\mbox{ }a,\mbox{ }R}$ \\ $\mathtt{\texttt{\char32}\mbox{ }/\mbox{ }a,\mbox{ }R}$\end{tabular}};
    \fill [black] (37.82,-12.92) -- (38.7,-12.57) -- (37.89,-11.98);
    \draw [black] (39.5,-15.6) -- (50.9,-15.6);
    \fill [black] (50.9,-15.6) -- (50.1,-15.1) -- (50.1,-16.1);
    \draw (45.2,-16.1) node [below] {$\mathtt{b\mbox{ }/\mbox{ }b,\mbox{ }R}$};
    \draw [black] (55.223,-18.28) arc (54:-234:2.25);
    \draw (53.9,-22.85) node [below] {$\mathtt{c\mbox{ }/\mbox{ }c,\mbox{ }R}$};
    \fill [black] (52.58,-18.28) -- (51.7,-18.63) -- (52.51,-19.22);
    \end{tikzpicture}

  \item
    \begin{tikzpicture}[scale=0.2,baseline={([yshift=-.5ex]current bounding box.center)}]
    \tikzstyle{every node}+=[inner sep=0pt]
    \draw [black] (19.1,-15.7) circle (3);
    \draw [black] (35.7,-15.7) circle (3);
    \draw [black] (19.1,-30) circle (3);
    \draw [black] (19.1,-30) circle (2.4);
    \draw [black] (35.7,-30) circle (3);
    \draw [black] (11.4,-15.7) -- (16.1,-15.7);
    \fill [black] (16.1,-15.7) -- (15.3,-15.2) -- (15.3,-16.2);
    \draw [black] (22.1,-15.7) -- (32.7,-15.7);
    \fill [black] (32.7,-15.7) -- (31.9,-15.2) -- (31.9,-16.2);
    \draw (27.4,-16.2) node [below] {$\mathtt{a\mbox{ }/\mbox{ }b,\mbox{ }R}$};
    \draw [black] (19.1,-18.7) -- (19.1,-27);
    \fill [black] (19.1,-27) -- (19.6,-26.2) -- (18.6,-26.2);
    \draw (19.6,-22.85) node [right] {$\mathtt{b\mbox{ }/\mbox{ }b,\mbox{ }R}$};
    \draw [black] (34.377,-13.02) arc (234:-54:2.25);
    \draw (35.7,-8.45) node [above] {$\mathtt{a\mbox{ }/\mbox{ }b,\mbox{ }R}$};
    \fill [black] (37.02,-13.02) -- (37.9,-12.67) -- (37.09,-12.08);
    \draw [black] (35.7,-18.7) -- (35.7,-27);
    \fill [black] (35.7,-27) -- (36.2,-26.2) -- (35.2,-26.2);
    \draw (36.2,-22.85) node [right] {$\mathtt{\texttt{\char32}\mbox{ }/\mbox{ }\texttt{\char32},\mbox{ }L}$};
    \draw [black] (37.023,-32.68) arc (54:-234:2.25);
    \draw (35.7,-37.25) node [below] {$\mathtt{b\mbox{ }/\mbox{ }b,\mbox{ }L}$};
    \fill [black] (34.38,-32.68) -- (33.5,-33.03) -- (34.31,-33.62);
    \draw [black] (32.7,-30) -- (22.1,-30);
    \fill [black] (22.1,-30) -- (22.9,-30.5) -- (22.9,-29.5);
    \draw (27.4,-29.5) node [above] {$\mathtt{\texttt{\char32}\mbox{ }/\mbox{ }\texttt{\char32},\mbox{ }L}$};
    \end{tikzpicture}

  \item
    \begin{tikzpicture}[scale=0.2,baseline={([yshift=-.5ex]current bounding box.center)}]
    \tikzstyle{every node}+=[inner sep=0pt]
    \draw [black] (26.1,-4.9) circle (3);
    \draw [black] (43.1,-4.9) circle (3);
    \draw [black] (43.1,-20.3) circle (3);
    \draw [black] (26.1,-20.3) circle (3);
    \draw [black] (10.6,-20.3) circle (3);
    \draw [black] (10.6,-20.3) circle (2.4);
    \draw [black] (18.4,-4.9) -- (23.1,-4.9);
    \fill [black] (23.1,-4.9) -- (22.3,-4.4) -- (22.3,-5.4);
    \draw [black] (29.1,-4.9) -- (40.1,-4.9);
    \fill [black] (40.1,-4.9) -- (39.3,-4.4) -- (39.3,-5.4);
    \draw (34.6,-4.4) node [above] {$\mathtt{a\mbox{ }/\mbox{ }b,\mbox{ }R}$};
    \draw [black] (45.78,-3.577) arc (144:-144:2.25);
    \draw (50.35,-4.9) node [right] {\begin{tabular}{c}$\mathtt{a\mbox{ }/\mbox{ }a,\mbox{ }R}$ \\ $\mathtt{b\mbox{ }/\mbox{ }b,\mbox{ }R}$\end{tabular}};
    \fill [black] (45.78,-6.22) -- (46.13,-7.1) -- (46.72,-6.29);
    \draw [black] (43.1,-7.9) -- (43.1,-17.3);
    \fill [black] (43.1,-17.3) -- (43.6,-16.5) -- (42.6,-16.5);
    \draw (43.6,-12.6) node [right] {$\mathtt{c\mbox{ }/\mbox{ }b,\mbox{ }L}$};
    \draw [black] (45.78,-18.977) arc (144:-144:2.25);
    \draw (50.35,-20.3) node [right] {\begin{tabular}{c}$\mathtt{a\mbox{ }/\mbox{ }a,\mbox{ }L}$ \\ $\mathtt{b\mbox{ }/\mbox{ }b,\mbox{ }L}$\end{tabular}};
    \fill [black] (45.78,-21.62) -- (46.13,-22.5) -- (46.72,-21.69);
    \draw [black] (40.1,-20.3) -- (29.1,-20.3);
    \fill [black] (29.1,-20.3) -- (29.9,-20.8) -- (29.9,-19.8);
    \draw (34.6,-20.8) node [below] {$\mathtt{\texttt{\char32}\mbox{ }/\mbox{ }\texttt{\char32},\mbox{ }R}$};
    \draw [black] (27.423,-22.98) arc (54:-234:2.25);
    \draw (26.1,-27.55) node [below] {$\mathtt{b\mbox{ }/\mbox{ }b,\mbox{ }R}$};
    \fill [black] (24.78,-22.98) -- (23.9,-23.33) -- (24.71,-23.92);
    \draw [black] (28.32,-18.29) -- (40.88,-6.91);
    \fill [black] (40.88,-6.91) -- (39.95,-7.08) -- (40.62,-7.82);
    \draw (30,-13.11) node [above] {$\mathtt{a\mbox{ }/\mbox{ }b,\mbox{ }R}$};
    \draw [black] (23.1,-20.3) -- (13.6,-20.3);
    \fill [black] (13.6,-20.3) -- (14.4,-20.8) -- (14.4,-19.8);
    \draw (18.35,-19.8) node [above] {$\mathtt{\texttt{\char32}\mbox{ }/\mbox{ }\texttt{\char32},\mbox{ }R}$};
    \end{tikzpicture}
\end{enumerate}

For exercise 8, give a brief \textbf{informal argument} that the language can be decided by a Turing machine. You do not have to draw a Turing machine diagram.

\begin{enumerate}[resume]
  \item $\{ \mathtt{ a^n\ |\ n}$ is prime$\}$
\end{enumerate}

\end{document}
