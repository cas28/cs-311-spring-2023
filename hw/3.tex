\documentclass[parskip]{scrartcl}
\usepackage[margin=1in]{geometry}
\usepackage[T1]{fontenc}
\usepackage{fancyvrb}
\usepackage{amsfonts}
\usepackage{enumitem}
\usepackage{tikz}
\usepackage{hyperref}
\usepackage{MnSymbol}

\subject{CS 311 Computational Structures}
\author{Katie Casamento}
\date{Spring 2023}
\publishers{Portland State University}

\title{Assignment 3}
\subtitle{NFAs and closure properties}

% straight quotes in \mathtt
\DeclareMathSymbol{\mathdblquotechar}{\mathalpha}{letters}{`"}
\newcommand{\mathdblquote}{\mathtt{\mathdblquotechar}}
\begingroup\lccode`~=`"\lowercase{\endgroup
  \let~\mathdblquote
}
\AtBeginDocument{\mathcode`"="8000 }

\begin{document}

\maketitle

Each exercise is worth three points, for a total of 36 points.

If you're working on paper, it's fine to ``scan'' your work with a phone camera. Just \textbf{make sure to set the lighting and contrast so that your writing is clearly readable}.

If you're working on a computer, you might find this tool helpful:

\quad \url{https://madebyevan.com/fsm/}

There is at least one useful feature of this tool that is not mentioned by the instructions on the webpage: you can click and drag the \textbf{middle} of an arrow to make it curved.

Feel free to submit your diagrams as \texttt{.png} files, just \textbf{make sure to name the files so that it's clear which exercise each image corresponds to}.

\section*{Exercises}

For each formal language definition in exercises 1-4, draw a diagram to define an NFA which recognizes the language.

  \begin{enumerate}[noitemsep]
    \item $\mathtt{\{ a^nb \} \cup \{ a^nb^n\ |\ n \le 3 \}}$
    \item $\mathtt{\{ a^nb^n\ |\ n \le 3 \} \cdot \{ b^nc^n\ |\ n \le 3 \}}$
    \item $\mathtt{\{ a^nb^n\ |\ n \le 3 \}^* }$
    \item $\mathtt{\{ a^nb^n\ |\ n \le 3 \}^* \cup \{ a^nb \}}$
  \end{enumerate}

For each NFA diagram in exercises 5-6, list \textbf{all} strings in the language recognized by the DFA.

  \begin{enumerate}[resume]
    \item
      \begin{tikzpicture}[scale=0.2,baseline={([yshift=-.5ex]current bounding box.center)}]
      \tikzstyle{every node}+=[inner sep=0pt]
      \draw [black] (21.8,-27.8) circle (3);
      \draw [black] (33.9,-27.8) circle (3);
      \draw [black] (21.8,-39.6) circle (3);
      \draw [black] (33.9,-39.6) circle (3);
      \draw [black] (33.9,-39.6) circle (2.4);
      \draw [black] (13.3,-27.8) -- (18.8,-27.8);
      \fill [black] (18.8,-27.8) -- (18,-27.3) -- (18,-28.3);
      \draw [black] (24.8,-27.8) -- (30.9,-27.8);
      \fill [black] (30.9,-27.8) -- (30.1,-27.3) -- (30.1,-28.3);
      \draw (27.85,-27.3) node [above] {$\mathtt{a}$};
      \draw [black] (21.8,-30.8) -- (21.8,-36.6);
      \fill [black] (21.8,-36.6) -- (22.3,-35.8) -- (21.3,-35.8);
      \draw (21.3,-33.7) node [left] {$\mathtt{a}$};
      \draw [black] (24.8,-39.6) -- (30.9,-39.6);
      \fill [black] (30.9,-39.6) -- (30.1,-39.1) -- (30.1,-40.1);
      \draw (27.85,-40.1) node [below] {$\mathtt{b}$};
      \draw [black] (33.9,-30.8) -- (33.9,-36.6);
      \fill [black] (33.9,-36.6) -- (34.4,-35.8) -- (33.4,-35.8);
      \draw (34.4,-33.7) node [right] {$\mathtt{c}$};
      \draw [black] (31.75,-29.89) -- (23.95,-37.51);
      \fill [black] (23.95,-37.51) -- (24.87,-37.3) -- (24.17,-36.59);
      \draw (26.77,-33.22) node [above] {$\mathtt{c}$};
      \end{tikzpicture}

    \item
      \begin{tikzpicture}[scale=0.2,baseline={([yshift=-.5ex]current bounding box.center)}]
      \tikzstyle{every node}+=[inner sep=0pt]
      \draw [black] (21.8,-27.8) circle (3);
      \draw [black] (21.8,-27.8) circle (2.4);
      \draw [black] (33.9,-27.8) circle (3);
      \draw [black] (48,-27.8) circle (3);
      \draw [black] (62.4,-27.8) circle (3);
      \draw [black] (62.4,-27.8) circle (2.4);
      \draw [black] (13.3,-27.8) -- (18.8,-27.8);
      \fill [black] (18.8,-27.8) -- (18,-27.3) -- (18,-28.3);
      \draw [black] (24.8,-27.8) -- (30.9,-27.8);
      \fill [black] (30.9,-27.8) -- (30.1,-27.3) -- (30.1,-28.3);
      \draw (27.85,-27.3) node [above] {$\mathtt{a,\varepsilon}$};
      \draw [black] (36.9,-27.8) -- (45,-27.8);
      \fill [black] (45,-27.8) -- (44.2,-27.3) -- (44.2,-28.3);
      \draw (40.95,-27.3) node [above] {$\mathtt{b,\varepsilon}$};
      \draw [black] (51,-27.8) -- (59.4,-27.8);
      \fill [black] (59.4,-27.8) -- (58.6,-27.3) -- (58.6,-28.3);
      \draw (55.2,-27.3) node [above] {$\mathtt{c}$};
      \end{tikzpicture}
  \end{enumerate}

For each NFA diagram in exercises 7-9, list \textbf{five} strings in the language recognized by the NFA.

  \begin{enumerate}[resume]
    \item 
      \begin{tikzpicture}[scale=0.2,baseline={([yshift=-.5ex]current bounding box.center)}]
      \tikzstyle{every node}+=[inner sep=0pt]
      \draw [black] (21.8,-27.8) circle (3);
      \draw [black] (21.8,-27.8) circle (2.4);
      \draw [black] (35.1,-27.8) circle (3);
      \draw [black] (13.3,-27.8) -- (18.8,-27.8);
      \fill [black] (18.8,-27.8) -- (18,-27.3) -- (18,-28.3);
      \draw [black] (23.123,-30.48) arc (54:-234:2.25);
      \draw (21.8,-35.05) node [below] {$\mathtt{a}$};
      \fill [black] (20.48,-30.48) -- (19.6,-30.83) -- (20.41,-31.42);
      \draw [black] (24.8,-27.8) -- (32.1,-27.8);
      \fill [black] (32.1,-27.8) -- (31.3,-27.3) -- (31.3,-28.3);
      \draw (28.45,-28.3) node [below] {$\mathtt{\varepsilon,b}$};
      \draw [black] (23.868,-25.652) arc (125.25484:54.74516:7.937);
      \fill [black] (23.87,-25.65) -- (24.81,-25.6) -- (24.23,-24.78);
      \draw (28.45,-23.7) node [above] {$\mathtt{\varepsilon}$};
      \draw [black] (36.423,-30.48) arc (54:-234:2.25);
      \draw (35.1,-35.05) node [below] {$\mathtt{c}$};
      \fill [black] (33.78,-30.48) -- (32.9,-30.83) -- (33.71,-31.42);
      \end{tikzpicture}

    \item
      \begin{tikzpicture}[scale=0.2,baseline={([yshift=-.5ex]current bounding box.center)}]
      \tikzstyle{every node}+=[inner sep=0pt]
      \draw [black] (17.4,-15.2) circle (3);
      \draw [black] (17.4,-15.2) circle (2.4);
      \draw [black] (26.7,-29.1) circle (3);
      \draw [black] (35.8,-15.2) circle (3);
      \draw [black] (35.8,-15.2) circle (2.4);
      \draw [black] (19.07,-17.69) -- (25.03,-26.61);
      \fill [black] (25.03,-26.61) -- (25,-25.66) -- (24.17,-26.22);
      \draw (21.44,-23.49) node [left] {$\mathtt{a}$};
      \draw [black] (28.34,-26.59) -- (34.16,-17.71);
      \fill [black] (34.16,-17.71) -- (33.3,-18.11) -- (34.14,-18.65);
      \draw (31.87,-23.47) node [right] {$\mathtt{b,\varepsilon}$};
      \draw [black] (32.8,-15.2) -- (20.4,-15.2);
      \fill [black] (20.4,-15.2) -- (21.2,-15.7) -- (21.2,-14.7);
      \draw (26.6,-15.7) node [below] {$\mathtt{c}$};
      \draw [black] (20.028,-13.762) arc (113.46929:66.53071:16.501);
      \fill [black] (33.17,-13.76) -- (32.64,-12.99) -- (32.24,-13.9);
      \draw (26.6,-11.9) node [above] {$\mathtt{\varepsilon}$};
      \draw [black] (9,-15.2) -- (14.4,-15.2);
      \fill [black] (14.4,-15.2) -- (13.6,-14.7) -- (13.6,-15.7);
      \end{tikzpicture}

    \item
      \begin{tikzpicture}[scale=0.2,baseline={([yshift=-.5ex]current bounding box.center)}]
      \tikzstyle{every node}+=[inner sep=0pt]
      \draw [black] (17.4,-15.2) circle (3);
      \draw [black] (17.4,-15.2) circle (2.4);
      \draw [black] (17.4,-30) circle (3);
      \draw [black] (35.8,-15.2) circle (3);
      \draw [black] (35.8,-30) circle (3);
      \draw [black] (35.8,-30) circle (2.4);
      \draw [black] (17.4,-18.2) -- (17.4,-27);
      \fill [black] (17.4,-27) -- (17.9,-26.2) -- (16.9,-26.2);
      \draw (16.9,-22.6) node [left] {$\mathtt{a}$};
      \draw [black] (9,-15.2) -- (14.4,-15.2);
      \fill [black] (14.4,-15.2) -- (13.6,-14.7) -- (13.6,-15.7);
      \draw [black] (20.4,-30) -- (32.8,-30);
      \fill [black] (32.8,-30) -- (32,-29.5) -- (32,-30.5);
      \draw (26.6,-30.5) node [below] {$\mathtt{\varepsilon}$};
      \draw [black] (35.8,-27) -- (35.8,-18.2);
      \fill [black] (35.8,-18.2) -- (35.3,-19) -- (36.3,-19);
      \draw (36.3,-22.6) node [right] {$\mathtt{\varepsilon}$};
      \draw [black] (20.4,-15.2) -- (32.8,-15.2);
      \fill [black] (32.8,-15.2) -- (32,-14.7) -- (32,-15.7);
      \draw (26.6,-15.7) node [below] {$\mathtt{a}$};
      \draw [black] (19.74,-17.08) -- (33.46,-28.12);
      \fill [black] (33.46,-28.12) -- (33.15,-27.23) -- (32.53,-28.01);
      \draw (24.66,-23.09) node [below] {$\mathtt{a,\varepsilon}$};
      \draw [black] (20.039,-13.782) arc (113.10542:66.89458:16.719);
      \fill [black] (20.04,-13.78) -- (20.97,-13.93) -- (20.58,-13.01);
      \draw (26.6,-11.94) node [above] {$\mathtt{b}$};
      \end{tikzpicture}
  \end{enumerate}

  \begin{enumerate}[noitemsep,resume]
    \item Draw a \textbf{DFA} that recognizes exactly the same language as the NFA in exercise 7.
    \item Draw a \textbf{DFA} that recognizes exactly the same language as the NFA in exercise 8.
  \end{enumerate}

  \begin{enumerate}[noitemsep,resume]
    \item
      Here is a definition of a set operator written $\cupdot$, which takes two sets as operands:

      \quad $A \cupdot B = A \cdot (A \cup B)$

      Give a short proof \textbf{sketch} to show that the set of regular languages is closed under the $\cupdot$ operator. Follow the format we've covered in lecture, where we've shown that the set of regular languages is closed under union ($\cup$), concatenation ($\cdot$), and repetition (${}^*$).
  \end{enumerate}


\end{document}
