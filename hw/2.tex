\documentclass[parskip]{scrartcl}
\usepackage[margin=1in]{geometry}
\usepackage[T1]{fontenc}
\usepackage{fancyvrb}
\usepackage{amsfonts}
\usepackage{enumitem}
\usepackage{tikz}
\usepackage{hyperref}

\subject{CS 311 Computational Structures}
\author{Katie Casamento}
\date{Spring 2023}
\publishers{Portland State University}

\title{Assignment 2}
\subtitle{Recognizing formal languages with DFAs}

% straight quotes in \mathtt
\DeclareMathSymbol{\mathdblquotechar}{\mathalpha}{letters}{`"}
\newcommand{\mathdblquote}{\mathtt{\mathdblquotechar}}
\begingroup\lccode`~=`"\lowercase{\endgroup
  \let~\mathdblquote
}
\AtBeginDocument{\mathcode`"="8000 }

\begin{document}

\maketitle

Each exercise is worth three points, for a total of 36 points.

If you're working on paper, it's fine to ``scan'' your work with a phone camera. Just \textbf{make sure to set the lighting and contrast so that your writing is clearly readable}.

If you're working on a computer, you might find this tool helpful:

\quad \url{https://madebyevan.com/fsm/}

There is at least one useful feature of this tool that is not mentioned by the instructions on the webpage: you can click and drag the \textbf{middle} of an arrow to make it curved.

Feel free to submit your DFA diagrams as \texttt{.png} files, just \textbf{make sure to name the files so that it's clear which exercise each image corresponds to}.

\section*{Exercises}

For each formal language definition in exercises 1-8, draw a diagram to define a DFA which recognizes the language. You may use the ``dead-end'' shorthand that we've discussed in lecture.

\begin{enumerate}[noitemsep]
  \item $\mathtt{\{ "a", "b", "ba" \}}$
  \item $\mathtt{\{ "", "a", "b", "ba" \}}$
  \item $\mathtt{\{ a^n\ |\ n < 3 \}}$
  \item $\mathtt{\{ a^nb\ |\ n < 3 \}}$
  \item $\mathtt{\{ a^nb\ |\ n > 3 \}}$
  \item $\mathtt{\{ ab^n\ |\ n > 3 \}}$
  \item $\mathtt{\{ a^mb^n\ |\ m < 3, n > 3 \}}$
  \item $\mathtt{\{ a^mb^n\ |\ m > 3, n > 3 \}}$
\end{enumerate}

For each DFA diagram in exercises 9-10, list \textbf{five} strings in the language recognized by the DFA.

\begin{enumerate}[resume]
  \item
    \begin{tikzpicture}[scale=0.2,baseline={([yshift=-.5ex]current bounding box.center)}]
    \tikzstyle{every node}+=[inner sep=0pt]
    \draw [black] (19.9,-26.7) circle (3);
    \draw [black] (19.9,-26.7) circle (2.4);
    \draw [black] (37.5,-26.7) circle (3);
    \draw [black] (11.7,-26.7) -- (16.9,-26.7);
    \fill [black] (16.9,-26.7) -- (16.1,-26.2) -- (16.1,-27.2);
    \draw [black] (18.577,-24.02) arc (234:-54:2.25);
    \draw (19.9,-19.45) node [above] {$b$};
    \fill [black] (21.22,-24.02) -- (22.1,-23.67) -- (21.29,-23.08);
    \draw [black] (22.9,-26.7) -- (34.5,-26.7);
    \fill [black] (34.5,-26.7) -- (33.7,-26.2) -- (33.7,-27.2);
    \draw (28.7,-27.2) node [below] {$a$};
    \end{tikzpicture}

  \item
    \begin{tikzpicture}[scale=0.2,baseline={([yshift=-.5ex]current bounding box.center)}]
    \tikzstyle{every node}+=[inner sep=0pt]
    \draw [black] (19.9,-26.7) circle (3);
    \draw [black] (38.6,-26.7) circle (3);
    \draw [black] (38.6,-11.9) circle (3);
    \draw [black] (38.6,-11.9) circle (2.4);
    \draw [black] (19.9,-11.9) circle (3);
    \draw [black] (19.9,-11.9) circle (2.4);
    \draw [black] (36.938,-29.185) arc (-42.06986:-137.93014:10.357);
    \fill [black] (36.94,-29.19) -- (36.03,-29.44) -- (36.77,-30.11);
    \draw (29.25,-33.1) node [below] {$a$};
    \draw [black] (21.842,-24.426) arc (131.77403:48.22597:11.119);
    \fill [black] (21.84,-24.43) -- (22.77,-24.27) -- (22.11,-23.52);
    \draw (29.25,-21.1) node [above] {$b$};
    \draw [black] (38.6,-23.7) -- (38.6,-14.9);
    \fill [black] (38.6,-14.9) -- (38.1,-15.7) -- (39.1,-15.7);
    \draw (39.1,-19.3) node [right] {$c$};
    \draw [black] (19.9,-23.7) -- (19.9,-14.9);
    \fill [black] (19.9,-14.9) -- (19.4,-15.7) -- (20.4,-15.7);
    \draw (20.4,-19.3) node [right] {$d$};
    \draw [black] (11.7,-26.7) -- (16.9,-26.7);
    \fill [black] (16.9,-26.7) -- (16.1,-26.2) -- (16.1,-27.2);
    \end{tikzpicture}
\end{enumerate}

For each DFA diagram in exercises 11-12, list \textbf{all} strings in the language recognized by the DFA.

\begin{enumerate}[resume]
  \item
    \begin{tikzpicture}[scale=0.2,baseline={([yshift=-.5ex]current bounding box.center)}]
    \tikzstyle{every node}+=[inner sep=0pt]
    \draw [black] (19.9,-26.7) circle (3);
    \draw [black] (37.5,-26.7) circle (3);
    \draw [black] (37.5,-26.7) circle (2.4);
    \draw [black] (19.9,-11.7) circle (3);
    \draw [black] (11.7,-26.7) -- (16.9,-26.7);
    \fill [black] (16.9,-26.7) -- (16.1,-26.2) -- (16.1,-27.2);
    \draw [black] (22.9,-26.7) -- (34.5,-26.7);
    \fill [black] (34.5,-26.7) -- (33.7,-26.2) -- (33.7,-27.2);
    \draw (28.7,-27.2) node [below] {$a$};
    \draw [black] (19.9,-23.7) -- (19.9,-14.7);
    \fill [black] (19.9,-14.7) -- (19.4,-15.5) -- (20.4,-15.5);
    \draw (20.4,-19.2) node [right] {$b$};
    \draw [black] (22.58,-10.377) arc (144:-144:2.25);
    \draw (27.15,-11.7) node [right] {$a$};
    \fill [black] (22.58,-13.02) -- (22.93,-13.9) -- (23.52,-13.09);
    \end{tikzpicture}

  \item
    \begin{tikzpicture}[scale=0.2,baseline={([yshift=-.5ex]current bounding box.center)}]
    \tikzstyle{every node}+=[inner sep=0pt]
    \draw [black] (19.9,-26.7) circle (3);
    \draw [black] (37.5,-26.7) circle (3);
    \draw [black] (37.5,-26.7) circle (2.4);
    \draw [black] (19.9,-11.7) circle (3);
    \draw [black] (19.9,-11.7) circle (2.4);
    \draw [black] (37.5,-11.7) circle (3);
    \draw [black] (54.7,-11.7) circle (3);
    \draw [black] (54.7,-11.7) circle (2.4);
    \draw [black] (54.7,-26.7) circle (3);
    \draw [black] (11.7,-26.7) -- (16.9,-26.7);
    \fill [black] (16.9,-26.7) -- (16.1,-26.2) -- (16.1,-27.2);
    \draw [black] (22.9,-26.7) -- (34.5,-26.7);
    \fill [black] (34.5,-26.7) -- (33.7,-26.2) -- (33.7,-27.2);
    \draw (28.7,-27.2) node [below] {$a$};
    \draw [black] (19.9,-23.7) -- (19.9,-14.7);
    \fill [black] (19.9,-14.7) -- (19.4,-15.5) -- (20.4,-15.5);
    \draw (20.4,-19.2) node [right] {$b$};
    \draw [black] (22.9,-11.7) -- (34.5,-11.7);
    \fill [black] (34.5,-11.7) -- (33.7,-11.2) -- (33.7,-12.2);
    \draw (28.7,-12.2) node [below] {$a$};
    \draw [black] (37.5,-23.7) -- (37.5,-14.7);
    \fill [black] (37.5,-14.7) -- (37,-15.5) -- (38,-15.5);
    \draw (38,-19.2) node [right] {$b$};
    \draw [black] (40.5,-11.7) -- (51.7,-11.7);
    \fill [black] (51.7,-11.7) -- (50.9,-11.2) -- (50.9,-12.2);
    \draw (46.1,-12.2) node [below] {$a$};
    \draw [black] (40.5,-26.7) -- (51.7,-26.7);
    \fill [black] (51.7,-26.7) -- (50.9,-26.2) -- (50.9,-27.2);
    \draw (46.1,-27.2) node [below] {$a$};
    \draw [black] (54.7,-23.7) -- (54.7,-14.7);
    \fill [black] (54.7,-14.7) -- (54.2,-15.5) -- (55.2,-15.5);
    \draw (55.2,-19.2) node [right] {$a$};
    \end{tikzpicture}
\end{enumerate}

\end{document}
