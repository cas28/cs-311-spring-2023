\documentclass{article}

\usepackage{parskip}
\usepackage{hyperref}
\usepackage[margin=0.75in]{geometry}
\usepackage{hhline}

\hypersetup{
  colorlinks=true,
  linkcolor=blue,
  urlcolor=blue,
}

\urlstyle{same}

\title{CS 311 Computational Structures}
\author{Instructor: Katie Casamento}
\date{Spring 2023 Syllabus}

\begin{document}
\maketitle

\section{Academic misconduct}

  Review PSU's rules at \url{https://www.pdx.edu/dos/academic-misconduct}.

  This is not an exhaustive list, but here are some relevant principles:

    \begin{itemize}
      \item Always properly cite any content (code or writing) that someone else created when you include it in your work.
      \item Never claim that you created content that someone else created; always cite all collaborators on any group work you participated in.
      \item Obey intellectual property laws and ethics: pay attention to LICENSE files!
    \end{itemize}

\section{Repository}

  Most of the material for this course will be tracked in a GitLab repository at \url{https://gitlab.cecs.pdx.edu/cas28/cs-311-spring-2023}. This will let you see the history of any changes that I may make to the course material throughout the quarter.

\section{Acknowledgement}

Most of the lecture slides used in this course were created by Andrew Black, Andrew Tolmach, and Tim Sheard.

\section{Course description}

All undergraduate course descriptions can be found at \url{https://www.pdx.edu/computer-science/undergraduate-courses}.

\section{Course staff}

  Please \href{mailto:cas28@pdx.edu,theod@pdx.edu}{email both of us} if you have a question!

  Katie Casamento \\
  Email: \href{mailto:cas28@pdx.edu}{cas28@pdx.edu} \\
  Office hours: Wednesday 4-5pm, FAB 115D or \url{https://pdx.zoom.us/j/87911961929 }

  FAB 115D is in the CS offices behind the fishbowl, near the bottom-right of page 9 in the building floor plan at \url{https://www.pdx.edu/buildings/campus-building/fourth-avenue-building} (where L120-00 is the fishbowl).

  Ted Cooper \\
  Email: \href{mailto:theod@pdx.edu}{theod@pdx.edu} \\
  Office hours: Wednesday 11am, in the fishbowl or \url{https://pdx.zoom.us/j/88384842328}.  If it's busy, folks there in person
will get help first.

\section{Course discussion forum}

  We'll be using the Zulip server hosted by the CAT at PSU for course discussion:

  \url{https://fishbowl.zulip.cs.pdx.edu/#narrow/stream/185-311-spring-2023}

\section{Lecture}

  Lecture is Tuesday and Thursday 2:00-3:50PM, in Karl Miller Center 470 or \url{https://pdx.zoom.us/j/89952145809}. Live attendance is not graded, but \textbf{you will likely fall behind if you don't keep up with the lecture material each week}.

  Lecture recordings will be available on Canvas within 48 hours of each live lecture. The recordings will only be available to students enrolled in this course for this quarter.

  If you attend the live lecture remotely, you may choose to have your camera on or off. Please keep your microphone muted when you're not talking so it doesn't pick up noise in the environment. It's hard over Zoom, but try your best not to interrupt people in discussions; use the Zoom "raise hand" button to ask to speak if someone else is speaking.

\section{Tools}

  You will need paper and pencil (and eraser), or any equivalent. (Be prepared to do a lot of erasing.)

  You will need to draw some complex diagrams, so a text editor alone won't be enough.

  If you work on real paper (or whiteboard, etc.) you will need a scanner or a high-resolution camera.\\
  Most phones and laptops made in the past few years have a good enough camera.

  You might have heard of \LaTeX, a fancy tool for creating fancy scientific documents.\\
  I use \LaTeX\ to create my material for this course, which is how I can write the fancy \LaTeX\ logo here.\\
  It's an important tool to learn if you're thinking of a career in research or academia.

  You are not expected to use \LaTeX\ on your assignments, or to care about it at all.\\
  If you're interested in learning it, however, this course can be a great opportunity for practice!\\
  I will share the \LaTeX\ sources (\texttt{.tex} files) for all documents I create in this course.

\section{Assignments}

  There will be four assignments. You will have at least two weeks to complete each assignment.

  You may use any tools and consult any sources while you work, as long as you cite your sources clearly.\\
  (With one exception: you may not consult the answers of another student in this course.)

  In assignment questions, your job is to convince me that \textbf{you understand the answer}.\\
  It is not enough to simply cite an answer that someone else wrote.

  You are not required to show your work, but you may get partial credit for it even if you get the answer wrong.

  Assignments will be submitted to Canvas in standard text, image, or document formats.

  \textbf{If you work in Microsoft Word, submit your work as a \emph{PDF} file, not a DOCX file.}\\
  Third-party tools like LibreOffice and Google Docs cannot always display diagrams in DOCX files correctly.\\
  I use tools like these, and it is very important that I can see your diagrams correctly when I'm grading.

\section{Exam}

  There will be a ``take-home'' final exam, covering the topics of all four assignments.

  The exam is essentially just another assignment. All of the same rules and permissions apply.

\section{Grading}

  The graded items are weighted as follows.

  \begin{tabular}{l|l}
    grade item&weight\\\hhline{=|=}
    assignment 1&10\%\\\hline
    assignment 2&20\%\\\hline
    assignment 3&20\%\\\hline
    assignment 4&20\%\\\hline
    final exam&30\%
  \end{tabular}

\section{Deadlines}

  See the course schedule document on Canvas for the release dates and due dates of the assigned coursework.

  The deadlines for the exams are each "hard due dates": you must submit by these dates in order to get credit for your work.

  The assignment deadlines are "soft due dates". If you submit an assignment by its soft due date, you will get a grade and feedback on your submission within one week of the soft due date.

  If you submit an assignment after the soft due date, I guarantee that your submission will be graded for full credit by the end of the quarter, but I'll grade it whenever it's convenient for me. That's the deal!

\section{Resubmissions}

  As an incentive to submit assignments "on time", you have a limited opportunity to resubmit for an updated grade after your submission has been graded. If you submit an assignment by its soft due date, then after you get back your grade, you may resubmit once for an updated grade. This does not apply to either exam, only the assignments.

  A resubmission has a hard due date of two weeks after the original soft due date. This leaves you with at least one week to rework your submission after receiving a grade and feedback on it.

  Resubmissions after this hard due date will not be graded, and if you don't submit the an assignment by its soft due date, then resubmissions will not be graded at all for that assignment.

\end{document}
